﻿using AutoMapper;
using CadastroClientes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CadastroClientes.ViewModel
{
    public class ClienteVM
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string DDD { get; set; }
        public string Numero { get; set; }
        public List<Cliente> Clientes { get; set; }
        public List<Telefone> Telefones { get; set; }


        public static Cliente ToModel(ClienteVM vm)
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<ClienteVM, Cliente>();
            });

            var model = Mapper.Map<ClienteVM, Cliente>(vm);
            Mapper.Reset();
            return model;
        }

        public static ClienteVM FromModel(Cliente model)
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<Cliente, ClienteVM>();
            });

            var vm = Mapper.Map<Cliente, ClienteVM>(model);

            vm.Clientes = new List<Cliente>();

            Mapper.Reset();
            return vm;
        }
    }
}
