﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CadastroClientes.Models
{
    public class Telefone
    {
        public int Id { get; set; }
        public int ClienteId { get; set; }
        public string DDD { get; set; }
        public string Numero { get; set; }
    }
}
