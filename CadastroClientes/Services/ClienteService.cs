﻿using CadastroClientes.Data;
using CadastroClientes.Models;
using CadastroClientes.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CadastroClientes.Services
{
    public class ClienteService
    {
        private readonly AppDbContext _context;

        public ClienteService(AppDbContext context)
        {
            _context = context;
        }


        public List<Cliente> FindAll()
        {

            return _context.Cliente.ToList();
        }

        public List<Telefone> FindAllTelefone()
        {

            return _context.Telefone.ToList();
        }

        public void Add(Cliente obj)
        {
            _context.Cliente.Add(obj);
            _context.SaveChanges();
        }

        public void AddNumber(Telefone obj)
        {
            _context.Telefone.Add(obj);
            _context.SaveChanges();
        }

        public Cliente FindById(int Id)
        {
            return _context.Cliente.FirstOrDefault(c => c.Id == Id);
        }

        public void Edit(Cliente cliente)
        {
            _context.Cliente.Update(cliente);
            _context.SaveChanges();

        }


        public void Remove(int Id)
        {
            var tel = _context.Telefone.Where(t=>t.ClienteId == Id);
            var obj = _context.Cliente.Find(Id);

            if (tel!=null)
                _context.Telefone.RemoveRange(tel);

            _context.Cliente.Remove(obj);
            _context.SaveChanges();

        }



    }
}
