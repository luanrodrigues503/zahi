﻿using CadastroClientes.Models;
using CadastroClientes.Services;
using CadastroClientes.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CadastroClientes.Controllers
{
    public class ClienteController : Controller
    {
        private ClienteService _clienteService;

        public ClienteController(ClienteService cs)
        {
            _clienteService = cs;
        }

        public IActionResult Index()
        {
            //listando todos os clientes 
            var vm = new ClienteVM();
            
            vm.Clientes = _clienteService.FindAll();
            vm.Telefones = _clienteService.FindAllTelefone();

            return View(vm);
        }

        [HttpPost]
        public IActionResult Create(ClienteVM obj)
        {
            //convertendo o modelo de visualizacao para o modelo de dados
            var cliente = ClienteVM.ToModel(obj);

            //persistindo os dados
            _clienteService.Add(cliente);

            return RedirectToAction("index");
        }

        public IActionResult Edit(int Id)
        {

            var cliente = _clienteService.FindById(Id);

            var vm = ClienteVM.FromModel(cliente);
            vm.Clientes = _clienteService.FindAll();

            if (cliente == null)
                return NotFound();

            return View(vm);
        }

        public IActionResult AddNumber(int Id)
        {
            var cliente = _clienteService.FindById(Id);

            var vm = ClienteVM.FromModel(cliente);
            vm.Clientes = _clienteService.FindAll();

            if (cliente == null)
                return NotFound();

            return View(vm);
        }

        [HttpPost]
        public IActionResult AddNumber(ClienteVM vm)
        {
            var telefone = new Telefone() {
                ClienteId = vm.Id,
                DDD = vm.DDD,
                Numero = vm.Numero
            };

            _clienteService.AddNumber(telefone);

            return RedirectToAction("index");
        }

        [HttpPost]
        public IActionResult Edit(ClienteVM vm)
        {
            var cliente = ClienteVM.ToModel(vm);

            _clienteService.Edit(cliente);

            return RedirectToAction("index");
        }

        [HttpPost]
        public IActionResult Delete(int Id)
        {

            var cliente = _clienteService.FindById(Id);

            if (cliente == null)
                return NotFound();

            _clienteService.Remove(Id);

            return RedirectToAction("index");
        }

    }
}
